@ECHO OFF

echo Running Ecosim

rem Get start time:
for /F "tokens=1-4 delims=:.," %%a in ("%time%") do (
   set /A "start=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
)
echo. 

rem The %~dp0 (that�s a zero) variable when referenced within a Windows batch file will expand to the drive letter and path of that batch file.
cd /d %~dp0 //

rem Clean up results
set RESULTS=results-summary.csv
RMDIR "runs\" /S /Q
IF EXIST %RESULTS% (del %RESULTS%)

rem Run rake
call modelkit rake
rem Gather results
call modelkit rake results

echo Finished Ecosim runs 

rem Get end time:
for /F "tokens=1-4 delims=:.," %%a in ("%time%") do (
   set /A "end=(((%%a*60)+1%%b %% 100)*60+1%%c %% 100)*100+1%%d %% 100"
)
	
rem Get elapsed time:
set /A elapsed=end-start

rem Show elapsed time:
set /A hh=elapsed/(60*60*100), rest=elapsed%%(60*60*100), mm=rest/(60*100), rest%%=60*100, ss=rest/100, cc=rest%%100
if %mm% lss 10 set mm=0%mm%
if %ss% lss 10 set ss=0%ss%
if %cc% lss 10 set cc=0%cc%
	
echo Elapsed Time: %hh%:%mm%:%ss%.%cc%


PAUSE

