# Ecosim

Ecosim is a standalone energy analysis tool focused specifically on multifamily Commercial Heat Pump Water Heater (CHPWH) systems. This tool was developed to model system performance as part of the qualifications of CHPWH systems in the Northwest Energy Efficiency Alliance�s Advanced Water Heater Specification.

## Requirements

- [Modelkit Catalyst v0.7.0](https://share.bigladdersoftware.com/files/modelkit-catalyst-0.7.0.exe) (only command line tools are essential): To generate CSE input files from templates.

## Running tests

Use `modelkit rake` command from the *analysis\test* directory.

Use `modelkit rake clean` to remove old runs.

Use `modelkit rake results` to aggregate results and compare to *ref-results-summary.csv*.

A batchfile, *autotest.bat*, is included to automate this process and compare the reference results to the new results.