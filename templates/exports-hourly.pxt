<%#INITIALIZE
#####################################################################
# Ecosim - CHPWH Simulation
# Copyright (C) 2022 Ecotope Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#####################################################################

parameter "run_name"
parameter "run_name_short"

# System Config
parameter "system_type", :default=>"Central" # "Central" | "In-Unit"
parameter "temperature_maintenance_loop_config", :default=>true # If the system has a temperature maintenance loop (recirculation loop) or not

# Primary Heater
parameter "primary_heater_type", :default=>"HPWH" # "HPWH" | "Electric Resistance" | "Gas"
parameter "primary_heater_multiplier", :default=>6 # number of identical primary water heaters

# Loop Heater
parameter "loop_heater_config", :default=>"None" #  "None" | "Swing" | "Parallel"
parameter "loop_heater_multiplier", :default=>1 # number of identical loop water heaters

# Buffer Space
parameter "include_buffer_space", :default=>false #  true/false switch, does building include a buffer space for the HPWH tank or source air?

# Turn on/off hourly export columns
parameter "add_meter_energy_exports", :default=>false # adds export columns for meter categories
parameter "add_dhwmeter_flow_exports", :default=>false # adds export columns for DHW meter flow rates at fixture and primary water heater

# Dwelling Unit Inputs
parameter "sum_units" # total number of apartment units

%>

<%
# Constants
kg_to_lb = 2.20462262184877580722973801345
m3_to_ft3 = 35.31466745978701159020323096717
ft3_to_gal = 7.48051932412962552345665757561872692
m3_to_gal = m3_to_ft3 * ft3_to_gal
density_water = 1000 * kg_to_lb / m3_to_gal # no CSE probe?
specific_heat_water = 1 # Btu/lb/F  # no CSE probe?

# Probe assignments for output calculations

temperature_inlet_cold = "@DHWHEATER[1].tInlet"
temperature_outlet_primary = "@DHWHEATER[1].tHWOut"
volume_flow_primary = "@DHWHEATER[1].draw"

# Primary system and In-unit tallys
if system_type == "Central"
      energy_in_primary = "%s * (@DHWHEATER[1].inElec + @DHWHEATER[1].inElecBU + @DHWHEATER[1].inElecXBU)" % [primary_heater_multiplier]
      energy_out_primary = "%s * %s * %s * (%s - %s)" % [volume_flow_primary, density_water, specific_heat_water, temperature_outlet_primary, temperature_inlet_cold]
elsif system_type == "In-Unit"
      heater_vol_probes = ""
      heater_input_probes = ""
      heater_output_probes = ""
      for i in 1..sum_units
            if i == 1
                  heater_vol_probes += "@DHWHEATER[#{i}].draw"
                  heater_input_probes += "@DHWHEATER[#{i}].inElec + @DHWHEATER[#{i}].inElecBU + @DHWHEATER[#{i}].inElecXBU"
                  heater_output_probes += "@DHWHEATER[#{i}].draw * (@DHWHEATER[#{i}].tHWOut  - @DHWHEATER[#{i}].tInlet)"
            else
                  heater_vol_probes += " + @DHWHEATER[#{i}].draw"
                  heater_input_probes += " + @DHWHEATER[#{i}].inElec + @DHWHEATER[#{i}].inElecBU + @DHWHEATER[#{i}].inElecXBU"
                  heater_output_probes += " + @DHWHEATER[#{i}].draw * (@DHWHEATER[#{i}].tHWOut  - @DHWHEATER[#{i}].tInlet)"
            end
      end
      volume_flow_primary = "%s * (%s)" % [primary_heater_multiplier, heater_vol_probes]
      energy_in_primary = "%s * (%s)" % [primary_heater_multiplier, heater_input_probes]
      energy_out_primary = "%s * %s * %s * (%s)" % [primary_heater_multiplier, density_water, specific_heat_water, heater_output_probes]
end

# cop_primary = energy_out_primary / energy_in_primary

# Temperature maintenance loop
if loop_heater_config != "None"
      temperature_outlet_auxillary = "@DHWLOOPHEATER[1].tHWOut"
      energy_in_loop_heater = "%s * (@DHWLOOPHEATER[1].inElec + @DHWLOOPHEATER[1].inElecBU + @DHWLOOPHEATER[1].inElecXBU)" % [loop_heater_multiplier]
      volume_flow_tm = "@DHWLOOPHEATER[1].draw"
else
      temperature_outlet_auxillary = 0
      energy_in_loop_heater = 0
      volume_flow_tm = 0
end
if temperature_maintenance_loop_config
      energy_loop_loss_net = "@DHWSYSRES[1].H.qLoop"
      energy_loop_loss = "@DHWLOOP[1].HRLL"
else
      energy_loop_loss_net = "0"
      energy_loop_loss = "0"
end

# Entire system
if temperature_maintenance_loop_config
      temperature_loop_return = "@DHWLOOP[1].tRL"
else
      temperature_loop_return = "0"
end
temperature_loop_supply = "@DHWSys[1].tUse"
temperature_mains = "@DHWSys[1].tInlet"
if temperature_maintenance_loop_config
      volume_flow_loop = "@DHWLOOP[1].flow"
else
      volume_flow_loop = "0"
end
volume_flow_draw = "@DHWmeter[DHWMtrWH].H.Total" # Hot water out of the mixing valve. They alll seem to reference the same draw.

energy_in_total = "%s + %s" % [energy_in_primary, energy_in_loop_heater]
energy_out_calc = "%s * %s * %s * ( (%s) - %s )" % [volume_flow_draw, density_water, specific_heat_water, temperature_loop_supply, temperature_mains]
# cop_system = (energy_out_calc + energy_loop_loss) / energy_in_total
# cop_fixture = (energy_dhw_load + energy_loop_loss) / energy_in_total
# energy_dhw_load = @DHWSYSRES[1].H.qLoad
# will post-process annual energy sums to calculate annual COP

# cop_temperature_maintenance = (energy_loop_loss + energy_out_swing_draws) / ( energy_in_loop_heater + max(0, cop_primary * energy_out_swing_draws )
# will post-process annual energy sums to calculate annual COP
energy_out_swing_draws =  "%s * %s * %s * (%s - %s)" % [volume_flow_tm, density_water, specific_heat_water, temperature_outlet_auxillary, temperature_outlet_primary]
%>

EXPORTFILE   "ExportsHourly-<%= run_name_short %>"
   xfFileName = "ExportsHourly.csv"    // Name of file to write simulation results to

   EXPORT "ExportDHWHourly-<%= run_name_short %>"
      exType = UDT
      exFreq = hour
      exDayBeg = Jan 1
      ExDayEnd = Dec 31
      exHeader = ColumnsOnly

      EXPORTCOL colVal=$dayOfYear colHead="Julien" colDec=0  //
      EXPORTCOL colVal=$month colHead="Mo" colDec=0      //
      EXPORTCOL colVal=$dayOfMonth colHead="Day" colDec=0      //
      EXPORTCOL colVal=$DOWH colHead="DOWH" colDec=0      //
      EXPORTCOL colVal=$hour colHead="Hour" colDec=0      //

      EXPORTCOL colVal=$tDbO colHead="tDbO [F]" colDec=1      //
      EXPORTCOL colVal=<%= temperature_mains %> colHead="Temperature Mains [F]" colDec=3
      EXPORTCOL colVal=@Weather.tgrnd colHead="Temperature Ground [F]" colDec=3

// Energy
      EXPORTCOL colVal=(@meter[MtrElec].H.DHW + @meter[MtrElec].H.DHWBU + @meter[MtrElec].H.Usr2 + @meter[MtrElec].H.DHWMFL)/3412 colHead="Energy In Total Hourly [kWh]" colDec=3
      EXPORTCOL colVal=(<%= energy_in_total %>)/3412 colHead="Energy In Heaters Hourly [kWh]" colDec=3
      EXPORTCOL colVal=(<%= energy_in_primary %>)/3412 colHead="Energy Primary Hourly [kWh]" colDec=3
<% if system_type == "In-Unit" %>
      EXPORTCOL colVal=(@DHWHEATER[1].inElec + @DHWHEATER[1].inElecBU + @DHWHEATER[1].inElecXBU)/3412 colHead="Energy In-Unit 1 Hourly [kWh]" colDec=3
<% end %>
      EXPORTCOL colVal=(<%= energy_in_loop_heater %>)/3412 colHead="Energy Auxiliary Hourly [kWh]" colDec=3
      EXPORTCOL colVal=@meter[MtrElec].H.Usr2/3412 colHead="Energy Miss Hourly [kWh]" colDec=3
      EXPORTCOL colVal=@meter[MtrElec].H.DHWMFL/3412 colHead="Energy Pump Hourly [kWh]" colDec=3
      EXPORTCOL colVal=(<%= energy_loop_loss_net %>)/3412 colHead="Net Energy Recirc Load Hourly [kWh]" colDec=3
      EXPORTCOL colVal=(<%= energy_loop_loss %>)/3412 colHead="Energy Loop Segment Pipes Loss [kWh]" colDec=3
<% if system_type == "Central" %>
      EXPORTCOL colVal=@DHWSYSRES[1].H.qLoad/3412 colHead="Energy DHW Load Hourly [kWh]" colDec=3
<% elsif system_type == "In-Unit" %>
      <%
      heater_probes = ""
      for i in 1..sum_units
            if i == 1
                  heater_probes += "@DHWSYSRES[#{i}].H.qLoad"
            else
                  heater_probes += " + @DHWSYSRES[#{i}].H.qLoad"
            end
      end
      # Bug on outlet water volume for primary heater so for inunit assign the hot water load as energy out primary
      # they should be equal anyways without a loop or a secondary heater
      energy_out_primary = heater_probes
      %>
      EXPORTCOL colVal=(<%= heater_probes %>)/3412 colHead="Energy DHW Load Hourly [kWh]" colDec=3
<% end %>
      EXPORTCOL colVal=(<%= energy_out_primary %>)/3412 colHead="Energy Out Primary Hourly [kWh]" colDec=3
      EXPORTCOL colVal=(<%= energy_out_calc %>)/3412 colHead="Energy Out System [kWh]" colDec=3

      //EXPORTCOL colVal=@DHWLOOP[1].HRBL/3412 colHead="Energy Branch Loss [kWh] (Current hour branch pipe loss, btu)" colDec=3

// Flow Rates
      EXPORTCOL colVal=<%= volume_flow_primary %> colHead="Volume Draw Primary [gal]" colDec=3
      EXPORTCOL colVal=<%= volume_flow_tm %> colHead="Volume Draw Temperature Maintenance [gal]" colDec=3
      EXPORTCOL colVal=<%= volume_flow_loop %> colHead="Volume Flow Loop [gal]" colDec=3
      EXPORTCOL colVal=<%= volume_flow_draw %> colHead="Volume Flow Draw [gal]" colDec=3

// Temperatures
<% if loop_heater_config != "None" %>
      EXPORTCOL colVal=@DHWLoopHeater[1].tInlet colHead="Temperature Inlet Loop Heater [F]" colDec=3
<% else %>
      EXPORTCOL colVal="N/A" colHead="Temperature Inlet Loop Heater [F]" colDec=3
<% end %>
      EXPORTCOL colVal=@DHWSys[1].tInlet  colHead="DHWSYS tInlet [F]" colDec=1      //
      EXPORTCOL colVal=<%= temperature_outlet_primary %> colHead="Temperature Outlet Primary [F]" colDec=3
      EXPORTCOL colVal=<%= temperature_outlet_auxillary %> colHead="Temperature Outlet Auxiliary [F]" colDec=3
      EXPORTCOL colVal=<%= temperature_loop_supply %> colHead="Temperature Loop Supply [F]" colDec=3
      EXPORTCOL colVal=<%= temperature_loop_return %> colHead="Temperature Loop Return [F]" colDec=3
<% if loop_heater_config == "Swing" %>
      EXPORTCOL colVal=(<%= energy_out_swing_draws %>)/3412 colHead="Swing Draw Energy Out [kWh]" colDec=3
<% end %>

<% if include_buffer_space %>
      EXPORTCOL colVal=@znres["Garage-zn"].H.tAir colHead="Buffer Space Air Temperature [F]" colDec=3
<% end %>

<% if add_meter_energy_exports %>
// Detailed Energy
      EXPORTCOL colVal=@meter[MtrElec].H.DHW/3412 colHead="DHW Elec Hourly [kWh]" colDec=3
      EXPORTCOL colVal=@meter[MtrElec].H.DHWBU/3412 colHead="DHWBU Elec Hourly [kWh]" colDec=3
      EXPORTCOL colVal=@meter[MtrElec].H.DHWMFL/3412 colHead="DHWMFL Elec Hourly [kWh]" colDec=3
      EXPORTCOL colVal=(@meter[MtrElec].h.DHW + @meter[MtrElec].h.DHWBU + @meter[MtrElec].h.Usr2 + @meter[MtrElec].h.DHWMFL)/3412 colHead="DHW Total Hourly [kWh]" colDec=3

      <% if system_type == "Central" %>
      EXPORTCOL colVal=@DHWHeater[1].inElec/3412 colHead="DHW Heater inElec Hourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[1].inElecBU/3412 colHead="DHW Heater inElecBU Hourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[1].inElecXBU/3412 colHead="DHW Heater inElecXBU Hourly [kWh]" colDec=3
      <% elsif system_type == "In-Unit" %>
            <% for i in 1..sum_units %>
      EXPORTCOL colVal=@DHWHeater[<%= i %>].inElec/3412 colHead="DHW Heater <%= i %> inElec Hourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[<%= i %>].inElecBU/3412 colHead="DHW Heater <%= i %> inElecBU Hourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[<%= i %>].inElecXBU/3412 colHead="DHW Heater <%= i %> inElecXBU Hourly [kWh]" colDec=3
            <% end %>
      <% end %>

      <% if loop_heater_config != "None" %>
      EXPORTCOL colVal=@DHWLoopHeater[1].inElec/3412 colHead="DHW Loop Heater inElec Hourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWLoopHeater[1].inElecBU/3412 colHead="DHW Loop Heater inElecBU Hourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWLoopHeater[1].inElecXBU/3412 colHead="DHW Loop Heater inElecXBU Hourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWLoopHeater[1].tInlet colHead="DHW Loop Heater T_inlet [F]" colDec=3
      <% end %>

      EXPORTCOL colVal=@meter[MtrNatGas].h.DHW/10000 colHead="DHW Gas [kBtu]" colDec=3
      EXPORTCOL colVal=@meter[MtrNatGas].h.DHWBU/10000 colHead="DHW Gas BU [kBtu]" colDec=3
<% end %>

<% if add_dhwmeter_flow_exports %>
// Flow rates of draws at fixtures
      EXPORTCOL colVal=@DHWmeter[DHWMtrFXMix].H.Faucet colHead="Faucet Fixture [gal]"
      EXPORTCOL colVal=@DHWmeter[DHWMtrFXMix].H.Shower colHead="Shower Fixture [gal]"
      EXPORTCOL colVal=@DHWmeter[DHWMtrFXMix].H.Bath colHead="Bath Fixture [gal]"
      EXPORTCOL colVal=@DHWmeter[DHWMtrFXMix].H.CWashr colHead="CWashr Fixture [gal]"
      EXPORTCOL colVal=@DHWmeter[DHWMtrFXMix].H.DWashr colHead="DWashr Fixture [gal]"

// Flow rates of draws from primary water heater
      EXPORTCOL colVal=@DHWmeter[DHWMtrWH].H.Faucet colHead="Faucet WH [gal]"
      EXPORTCOL colVal=@DHWmeter[DHWMtrWH].H.Shower colHead="Shower WH [gal]"
      EXPORTCOL colVal=@DHWmeter[DHWMtrWH].H.Bath colHead="Bath WH [gal]"
      EXPORTCOL colVal=@DHWmeter[DHWMtrWH].H.CWashr colHead="CWashr WH [gal]"
      EXPORTCOL colVal=@DHWmeter[DHWMtrWH].H.DWashr colHead="DWashr WH [gal]"
      EXPORTCOL colVal=@DHWmeter[DHWMtrWH].H.Total colHead="Tot WH [gal]"
<% end %>
