<%#INITIALIZE
#####################################################################
# Ecosim - CHPWH Simulation
# Copyright (C) 2022 Ecotope Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#####################################################################

parameter "run_name", :default=>"mf88"
parameter "run_name_short"

# System Config
parameter "system_type", :default=>"Central" # "Central" | "In-Unit"
parameter "temperature_maintenance_loop_config", :default=>true # If the system has a temperature maintenance loop (recirculation loop) or not

# Primary Heater
parameter "primary_heater_type", :default=>"HPWH" # "HPWH" | "Electric Resistance" | "Gas"

# Loop Heater
parameter "loop_heater_config", :default=>"None" #  "None" | "Swing" | "Parallel"

# Turn on/off subhourly export columns
parameter "add_meter_energy_exports", :default=>false # adds export columns for meter categories

# Dwelling Unit Inputs
parameter "sum_units" # total number of apartment units

%>

EXPORTFILE   "ExportsSubhourly-<%= run_name_short %>"
   xfFileName = "ExportsSubhourly.csv"    // Name of file to write simulation results to

   EXPORT "ExportDHWSubhourly-<%= run_name_short %>"
      exType = UDT
      exFreq = subhour
      exDayBeg = Jan 1
      ExDayEnd = Dec 31
      exHeader = ColumnsOnly

      EXPORTCOL colVal=$dayOfYear colHead="Julien" colDec=0  //
      EXPORTCOL colVal=$month colHead="Mo" colDec=0      //
      EXPORTCOL colVal=$dayOfMonth colHead="Day" colDec=0      //
      EXPORTCOL colVal=$DOWH colHead="DOWH" colDec=0      //
      EXPORTCOL colVal=$hour colHead="Hour" colDec=0      //
      EXPORTCOL colVal=$subhour colHead="Subhour" colDec=0      //

// Energy
      EXPORTCOL colVal=@DHWSYSRES[1].S.qLoad/3412 colHead="Energy DHW Load Subhourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWSYSRES[1].S.qLoop/3412 colHead="Energy Recirc Load Subhourly [kWh]" colDec=3

// Temperatures
      EXPORTCOL colVal=@DHWSys[1].tOutPrimLT colHead="DHW Sys tOutPrimLT Subhourly [F]" colDec=3

<% if add_meter_energy_exports %>
// Detailed Energy
      <% if system_type == "Central" %>
      EXPORTCOL colVal=@DHWHeater[1].inElecSh/3412 colHead="DHW Heater inElec Subhourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[1].inElecBUSh/3412 colHead="DHW Heater inElecBU Subhourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[1].inElecXBUSh/3412 colHead="DHW Heater inElecXBU Subhourly [kWh]" colDec=3
      <% elsif system_type == "In-Unit" %>
            <% for i in 1..sum_units %>
      EXPORTCOL colVal=@DHWHeater[<%= i %>].inElecSh/3412 colHead="DHW Heater <%= i %> inElec Subhourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[<%= i %>].inElecBUSh/3412 colHead="DHW Heater <%= i %> inElecBU Subhourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[<%= i %>].inElecXBUSh/3412 colHead="DHW Heater <%= i %> inElecXBU Subhourly [kWh]" colDec=3
            <% end %>
      <% end %>
      <% if loop_heater_config != "None" %>
      EXPORTCOL colVal=@DHWLoopHeater[1].inElecSh/3412 colHead="DHW Loop Heater inElec Subhourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWLoopHeater[1].inElecBUSh/3412 colHead="DHW Loop Heater inElecBU Subhourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWLoopHeater[1].inElecXBUSh/3412 colHead="DHW Loop Heater inElecXBU Subhourly [kWh]" colDec=3
      <% end %>
      <% if primary_heater_type == "HPWH" %>
            <% if system_type == "Central" %>
      EXPORTCOL colVal=@DHWHeater[1].HPWH.inElec[0]/3412 colHead="DHW Heater HPWH.inElec[0] Subhourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[1].HPWH.inElec[1]/3412 colHead="DHW Heater HPWH.inElec[1] Subhourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[1].HPWH.tHWOut colHead="DHW Heater HPWH.tHWOut Subhourly [F]" colDec=3
            <% elsif system_type == "In-Unit" %>
                  <% for i in 1..sum_units %>
      EXPORTCOL colVal=@DHWHeater[#{i}].HPWH.inElec[0]/3412 colHead="DHW Heater #{i} HPWH.inElec[0] Subhourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[#{i}].HPWH.inElec[1]/3412 colHead="DHW Heater #{i} HPWH.inElec[1] Subhourly [kWh]" colDec=3
      EXPORTCOL colVal=@DHWHeater[#{i}].HPWH.tHWOut colHead="DHW Heater #{i} HPWH.tHWOut Subhourly [F]" colDec=3
                  <% end %>
            <% end %>
      <% end %>
<% end %>